define ubuntu_locale::fix($locale = $title) {
	exec { "update-locale $locale":
		command => "/usr/sbin/update-locale LANG=${locale} LANGUAGE=${locale} LC_ALL=${locale}",
        onlyif => "/usr/bin/test -x /usr/sbin/update-locale",
	}
}
